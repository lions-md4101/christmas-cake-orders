from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("", include("cakes_app.urls")),
    path("grtasfl/", include("massadmin.urls")),
    path("grtasfl/", admin.site.urls),
]
