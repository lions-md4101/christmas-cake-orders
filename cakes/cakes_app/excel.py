from io import BytesIO
import xlsxwriter

TITLE_HEIGHT = 40
NORMAL_HEIGHT = 25
DEFAULT_WIDTH = 20


class ExcelWorkbook:
    def __init__(self, dt, filename):
        self.filename = filename
        self.io = BytesIO()
        self.workbook = xlsxwriter.Workbook(self.io, {"in_memory": True})
        # remove any sheets in the newly created book to ensure a clean slate
        self.dt = dt
        self.active_sheet = None
        self.header_cell_format = self.workbook.add_format()
        self.header_cell_format.set_bold()
        self.header_cell_format.set_border(1)  # thin continuous
        self.header_cell_format.set_align("center")
        self.header_cell_format.set_align("vcenter")
        self.normal_cell_format = self.workbook.add_format()
        self.normal_cell_format.set_border(1)  # thin continuous
        self.normal_cell_format.set_align("left")
        self.normal_cell_format.set_align("bottom")

    def add_sheet(self, title, headers, row_height, widths={}):
        # with warnings.catch_warnings():
        #     warnings.simplefilter("ignore")
        #     # ignore warnings, to suppress message about overly-long titles
        self.active_sheet = self.workbook.add_worksheet(name=title)
        self.active_sheet.set_paper = 9  # A4
        self.active_sheet.set_margins(0.1, 0.1, 0.1, 0.1)
        title = f"{title} as at {self.dt:%H:%M on %Y/%m/%d}"
        # self.active_sheet.set_header(f"&C{title}")
        self.active_sheet.repeat_rows(0, 1)
        self.active_sheet.write("A1", title)
        self.row_num = 1
        for n, value in enumerate(headers):
            self.active_sheet.set_column(n, n, widths.get(value, DEFAULT_WIDTH))
            self.active_sheet.write(self.row_num, n, value, self.header_cell_format)
        self.active_sheet.set_row(0, TITLE_HEIGHT)
        self.active_sheet.set_row(1, TITLE_HEIGHT)
        self.row_height = row_height

    def add_registrees_sheet(self, title, headers, widths={}):
        self.add_sheet(title, headers, NORMAL_HEIGHT, widths)

    def add_list_sheet(self, title, headers, widths={}):
        self.add_sheet(title, headers, TITLE_HEIGHT, widths)

    def add_row(self, values):
        self.row_num += 1
        for col_num, value in enumerate(values, 0):
            self.active_sheet.write(
                self.row_num, col_num, value, self.normal_cell_format
            )
        self.active_sheet.set_row(self.row_num, self.row_height)

    def save(self):
        self.workbook.close()


if __name__ == "__main__":
    from datetime import datetime

    wb = ExcelWorkbook(datetime.now(), "test.xlsx")
    wb.add_registrees_sheet(
        "First But With Long Name!!",
        ["abc", "def", "ghi"],
        widths={"def": 100},
    )
    wb.add_row(["1", "2", "34567890"])
    wb.add_registrees_sheet("Second", ["ab", "cdef", "gh"], widths={"gh": 50})
    wb.save()
