from django.contrib import admin

from .models import (
    OrderWindow,
    DeliveryDate,
    DeliveryLocation,
    District,
    Order,
    Complaint,
)


class OrderAdmin(admin.ModelAdmin):
    list_filter = ["emailed", "order_date", "scheduled_despatch"]


admin.site.register(OrderWindow)
admin.site.register(DeliveryDate)
admin.site.register(DeliveryLocation)
admin.site.register(District)
admin.site.register(Order, OrderAdmin)
admin.site.register(Complaint)
