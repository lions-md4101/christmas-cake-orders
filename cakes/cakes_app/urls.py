from django.urls import path

from . import views

app_name = "cakes_app"
urlpatterns = [
    path("orders/", views.orders),
    path("orders/<int:year>/", views.orders),
    path("order_details/<int:order_id>", views.order_details),
    path("order/<str:district_fragment>/", views.order, name="order"),
    path("complaint/<str:district_fragment>/", views.complaint, name="complaint"),
    path("unemailed/", views.unemailed_list, name="unemailed"),
    path("despatch/", views.despatch_list, name="despatch"),
    path("despatch/<int:year>/", views.despatch_list, name="despatch"),
    path(
        "despatch_details/<str:despatch_date>/<int:location_id>", views.despatch_details
    ),
    path("excel/", views.excel_download),
    path("excel/<int:year>/", views.excel_download),
]
