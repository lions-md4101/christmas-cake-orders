from django.db import models
from django.utils import timezone


class OrderWindow(models.Model):
    open = models.BooleanField("Window Open", default=False)
    closed_message = models.CharField("Closed Message", max_length=200)

    def __str__(self):
        return "Order Window"


class DeliveryDate(models.Model):
    description = models.CharField(max_length=50)
    sequence = models.IntegerField("Presentation Order")

    def __str__(self):
        return self.description


class District(models.Model):
    name = models.CharField(max_length=10)
    chair_name = models.CharField("Cake Chair", max_length=100)
    chair_email = models.EmailField("Cake Chair Email")
    chair_cell = models.CharField(
        "Cake Chair Cell Number", max_length=20, default=None, null=True, blank=True
    )
    treasurer_email = models.EmailField("District Treasurer Email")
    dg_email = models.EmailField("Governor Email")

    def __str__(self):
        return self.name


class DeliveryLocation(models.Model):
    name = models.CharField("Location Name", max_length=100)
    address = models.CharField("Address", max_length=200)
    district = models.ForeignKey(
        District,
        on_delete=models.CASCADE,
    )
    active = models.BooleanField("Active", default=False)

    def __str__(self):
        return self.name


class Order(models.Model):
    order_date = models.DateTimeField("Order Date", default=timezone.now)
    district = models.ForeignKey(
        District,
        on_delete=models.CASCADE,
    )
    delivery_date = models.ForeignKey(
        DeliveryDate,
        on_delete=models.CASCADE,
        verbose_name="Approximate Date Cakes are Required",
    )
    delivery_location = models.ForeignKey(
        DeliveryLocation,
        on_delete=models.CASCADE,
        verbose_name="Collection Point",
    )
    club_name = models.CharField("Club Name", max_length=100)
    num_1kg_cases = models.PositiveIntegerField(
        "Number of cases of 1kg cakes required (X12 cakes)"
    )
    num_500g_cases = models.PositiveIntegerField(
        "Number of cases of 500g cakes required (X12 cakes)"
    )
    resp_member_name = models.CharField(
        "Responsible Member - Name and Surname", max_length=100
    )
    resp_member_email = models.EmailField("Responsible Member - Email Address")
    resp_member_cell = models.CharField(
        "Responsible Member - Cell Number", max_length=20
    )
    pres_name = models.CharField("Club President - Name and Surname", max_length=100)
    pres_email = models.EmailField("Club President - Email Address")
    pres_cell = models.CharField("Club President - Cell Number", max_length=20)
    terms_acceptance = models.BooleanField(
        "I have read through and acknowledge the payment terms (see below form)",
        default=False,
    )
    complaints_acceptance = models.BooleanField(
        "I have read through and acknowledge the complaints procedure (see below form)",
        default=False,
    )
    comments = models.CharField(
        "Any further comments/requests", max_length=200, blank=True
    )
    emailed = models.BooleanField("Details Emailed", default=False)
    scheduled_despatch = models.DateField(
        "Scheduled Despatch Date", default=None, null=True, blank=True
    )

    def __str__(self):
        try:
            return f"{self.club_name} - {self.delivery_date} ({self.order_date.year}) - {self.num_1kg_cases} 1kg - {self.num_500g_cases} 500g"
        except Exception:
            return "New empty form"

    class Meta:
        ordering = ["delivery_date", "club_name"]


class Complaint(models.Model):
    complaint_date = models.DateTimeField("Order Date", default=timezone.now)
    district = models.ForeignKey(
        District,
        on_delete=models.CASCADE,
    )
    club_name = models.CharField("Club Name", max_length=100)
    resp_member_name = models.CharField(
        "Responsible Member - Name and Surname", max_length=100
    )
    resp_member_email = models.EmailField("Responsible Member - Email Address")
    resp_member_cell = models.CharField(
        "Responsible Member - Cell Number", max_length=20
    )
    batch_number = models.CharField(
        "Batch number of the affected cakes", max_length=40, blank=True
    )
    description = models.CharField("Complaint description", max_length=200, blank=True)

    def __str__(self):
        try:
            return f"{self.club_name} - {self.complaint_date}"
        except Exception:
            return "New empty form"

    class Meta:
        ordering = ["complaint_date", "club_name"]
