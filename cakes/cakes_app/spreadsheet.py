from .models import Order

from collections import defaultdict
from datetime import datetime
from . import excel

WIDTHS = {
    "Club Name": 30,
    "Delivery Location": 60,
    "Responsible Member": 30,
    "Club President": 30,
    "Additional Comments": 100,
}


def get_data(year: int):
    rows = []
    for order in Order.objects.filter(order_date__year=year):
        rows.append(
            {
                "Order Date": order.order_date.strftime("%y/%m/%d %H:%M"),
                "Club Name": order.club_name,
                "1kg Cases": order.num_1kg_cases,
                "500g Cases": order.num_500g_cases,
                "Delivery Date": order.delivery_date.description,
                "Delivery Location": f"{order.delivery_location.name} - {order.delivery_location.address}",
                "Responsible Member": order.resp_member_name,
                "Responsible Member Email": order.resp_member_email,
                "Responsible Member Cell": order.resp_member_cell,
                "Club President": order.pres_name,
                "Club President Email": order.pres_email,
                "Club President Cell": order.pres_cell,
                "District": order.district.name,
                "Additional Comments": order.comments,
            }
        )
    return rows


def sort_data(year: int):
    rows = get_data(year)
    d = defaultdict(list)
    for row in rows:
        d["all"].append(row)
        d[row["District"]].append(row)
    return d


def make_excel(year: int):
    dicts = sort_data(year)
    now = datetime.now()
    fn = f"{now:%y%m%dT%H%M}_lions_christmas_cake_orders.xlsx"
    wb = excel.ExcelWorkbook(now, fn)
    for title, d in dicts.items():
        wb.add_sheet(
            "All Orders" if title == "all" else f"District {title} Orders",
            list(d[0].keys()),
            widths=WIDTHS,
            row_height=25,
        )
        for row in d:
            wb.add_row(row.values())
        wb.add_row([""])
        wb.add_row(["Total number of 1kg cases:", sum([row["1kg Cases"] for row in d])])
        wb.add_row(
            ["Total number of 500g cases:", sum([row["500g Cases"] for row in d])]
        )
        wb.add_row(["Number of orders:", len(d)])
    wb.save()
    return wb
