# Generated by Django 4.2.2 on 2023-09-30 10:04

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("cakes_app", "0009_deliverydate_sequence"),
    ]

    operations = [
        migrations.CreateModel(
            name="OrderWindow",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "open",
                    models.BooleanField(default=False, verbose_name="Window Open"),
                ),
                (
                    "close_message",
                    models.CharField(max_length=200, verbose_name="Closed Message"),
                ),
            ],
        ),
    ]
