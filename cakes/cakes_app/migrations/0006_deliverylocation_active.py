# Generated by Django 4.2.1 on 2023-06-01 09:28

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("cakes_app", "0005_rename_email_district_chair_email_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="deliverylocation",
            name="active",
            field=models.BooleanField(default=False, verbose_name="Active"),
        ),
    ]
