from attrs import define
from basicauth.decorators import basic_auth_required
from django.forms import ModelForm, CheckboxInput, TextInput, Textarea
from django.http import FileResponse
from django.shortcuts import render

from collections import defaultdict
from datetime import date

from .models import (
    Order,
    District,
    DeliveryDate,
    DeliveryLocation,
    OrderWindow,
    Complaint,
)
from .spreadsheet import make_excel


@define()
class CakeTypes:
    t1kg: int = 0
    t500g: int = 0

    def __add__(self, other):
        self.t1kg += other.t1kg
        self.t500g += other.t500g
        return self

    def __str__(self):
        return f"1kg: {self.t1kg}; 500g: {self.t500g}"


def order(request, district_fragment):
    district = District.objects.get(name=district_fragment.upper())
    window = OrderWindow.objects.get(pk=1)
    if not window.open:
        return render(
            request,
            "order_closed.html",
            {
                "message": window.closed_message,
                "district": district,
            },
        )

    class OrderForm(ModelForm):
        class Meta:
            model = Order
            fields = [
                "delivery_date",
                "delivery_location",
                "club_name",
                "num_1kg_cases",
                "num_500g_cases",
                "resp_member_name",
                "resp_member_email",
                "resp_member_cell",
                "pres_name",
                "pres_email",
                "pres_cell",
                "terms_acceptance",
                "complaints_acceptance",
                "comments",
            ]
            widgets = {
                "resp_member_name": TextInput(attrs={"size": 40}),
                "resp_member_email": TextInput(attrs={"size": 40}),
                "pres_name": TextInput(attrs={"size": 40}),
                "pres_email": TextInput(attrs={"size": 40}),
                "club_name": TextInput(attrs={"size": 40}),
                "terms_acceptance": CheckboxInput(attrs={"required": True}),
                "complaints_acceptance": CheckboxInput(attrs={"required": True}),
                "comments": TextInput(attrs={"size": 60}),
            }

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields["delivery_location"].queryset = DeliveryLocation.objects.filter(
                district=district, active=True
            )

    if request.method == "POST":
        form = OrderForm(request.POST)
        order = form.save(commit=False)
        order.district = district
        order.save()
        return render(
            request,
            "order_complete.html",
            {
                "order": order,
                "num_1kg_cakes": order.num_1kg_cases * 12,
                "num_500g_cakes": order.num_500g_cases * 12,
            },
        )
    else:
        form = OrderForm()

        return render(
            request,
            "order_form.html",
            {
                "form": form,
                "district": district,
            },
        )


def complaint(request, district_fragment):
    district = District.objects.get(name=district_fragment.upper())

    class ComplaintForm(ModelForm):
        class Meta:
            model = Complaint
            fields = [
                "club_name",
                "resp_member_name",
                "resp_member_email",
                "resp_member_cell",
                "batch_number",
                "description",
            ]
            widgets = {
                "resp_member_name": TextInput(attrs={"size": 40}),
                "resp_member_email": TextInput(attrs={"size": 40}),
                "club_name": TextInput(attrs={"size": 40}),
                "batch_number": TextInput(attrs={"size": 40}),
                "description": Textarea(attrs={"size": 200}),
            }

    if request.method == "POST":
        form = ComplaintForm(request.POST)
        complaint = form.save(commit=False)
        complaint.district = district
        complaint.save()
        return render(
            request,
            "complaint_complete.html",
            {"complaint": complaint},
        )
    else:
        form = ComplaintForm()

        return render(
            request,
            "complaint_form.html",
            {
                "form": form,
                "district": district,
            },
        )


@basic_auth_required
def unemailed_list(request):
    orders = Order.objects.filter(emailed=False)
    return render(
        request,
        "unemailed_list.html",
        {"orders": orders},
    )


@basic_auth_required
def order_details(request, order_id):
    order = Order.objects.get(id=order_id)
    return render(
        request,
        "order_email.html",
        {
            "order": order,
            "num_1kg_cakes": order.num_1kg_cases * 12,
            "num_500g_cakes": order.num_500g_cases * 12,
        },
    )


@basic_auth_required
def despatch_list(request, year=None):
    if year is None:
        year = date.today().year
    orders = Order.objects.filter(
        scheduled_despatch__isnull=False, scheduled_despatch__year=year
    ).order_by("scheduled_despatch")
    d = {}
    for order in orders:
        dt = f"{order.scheduled_despatch:%Y-%m-%d}"
        d.setdefault(dt, {})
        d[dt].setdefault(order.delivery_location.id, order.delivery_location.name)
    print(d)

    return render(
        request,
        "despatch_list.html",
        {"year": year, "despatches": d},
    )


@basic_auth_required
def despatch_details(request, despatch_date, location_id):
    dt = date.fromisoformat(despatch_date)
    location = DeliveryLocation.objects.get(id=location_id)
    orders = Order.objects.filter(
        delivery_location=location,
        scheduled_despatch=dt,
    )
    emails = []
    clubs = []
    for n, order in enumerate(orders, 1):
        if order.resp_member_email:
            emails.append(order.resp_member_email)
        if order.pres_email:
            emails.append(order.pres_email)
        if n == 1:
            emails.append(order.district.chair_email)
            emails.append(order.district.dg_email)
            emails.append(order.district.treasurer_email)
            district_name = order.district.name
        clubs.append(
            f"{order.club_name} - {order.num_1kg_cases} 1kg cases; {order.num_500g_cases} 500g cases"
        )
    emails = set(emails)
    return render(
        request,
        "despatch_email.html",
        {
            "emails": ";".join(emails),
            "clubs": clubs,
            "location": location,
            "date": f"{dt:%d %B}",
            "district_name": district_name,
        },
    )


@basic_auth_required
def orders(request, year=None):
    if year is None:
        year = date.today().year
    orders = Order.objects.filter(order_date__year=year)
    delivery_dates = dict(
        (d.description, d.sequence) for d in DeliveryDate.objects.all()
    )

    dates = defaultdict(CakeTypes)
    districts = defaultdict(CakeTypes)
    clubs = defaultdict(lambda: defaultdict(CakeTypes))
    points = defaultdict(lambda: defaultdict(CakeTypes))
    total = CakeTypes()
    for order in orders:
        ct = CakeTypes(t1kg=order.num_1kg_cases, t500g=order.num_500g_cases)
        districts[order.district] += ct
        dates[order.delivery_date.description] += ct
        clubs[order.district][order.club_name] += ct
        points[order.district][order.delivery_location] += ct
        total += ct

    dates = list(dates.items())
    dates.sort(key=lambda x: delivery_dates[x[0]])
    keys = list(clubs.keys())
    keys.sort(key=lambda x: x.name)
    clubs_list = []
    for district in keys:
        l = list(clubs[district].items())
        l.sort()
        clubs_list.append((district, l))
    districts = list(districts.items())
    districts.sort(key=lambda x: x[0].name)
    keys = list(points.keys())
    keys.sort(key=lambda x: x.name)
    points_list = []
    for district in keys:
        l = list(points[district].items())
        l.sort(key=lambda x: x[0].name)
        points_list.append((district, l))

    return render(
        request,
        "orders.html",
        {
            "year": year,
            "total": total,
            "dates": dates,
            "districts": districts,
            "clubs": clubs_list,
            "points": points_list,
        },
    )


@basic_auth_required
def excel_download(request, year: int = None):
    if year is None:
        year = date.today().year
    wb = make_excel(year)
    wb.io.seek(0)
    return FileResponse(wb.io, as_attachment=True, filename=wb.filename)
