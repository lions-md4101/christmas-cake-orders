FROM registry.gitlab.com/kimvanwyk/python3-poetry:latest

COPY ./cakes/ /app/
COPY run.sh /app

VOLUME /storage

ENV STORAGE_DIR=/storage

ENTRYPOINT ["bash", "run.sh"]
